from django import forms

LIST_PROVINSI = [
    ('aceh', 'Aceh'),
    ('bali', 'Bali'),
    ('banten', 'Banten'),
    ('bengkulu', 'Bengkulu'),
    ('gorontalo', 'Gorontalo'),
    ('jakarta', 'Jakarta'),
    ('jambi', 'Jambi'),
    ('jawa_barat', 'Jawa Barat'),
    ('jawa_tengah', 'Jawa Tengah'),
    ('jawa_timur', 'Jawa Timur'),
    ('kalimantan_barat', 'Kalimantan Barat'),
    ('kalimantan_timur', 'Kalimantan Timur'),
    ('kalimantan_utara', 'Kalimantan Utara'),
    ('bangka_belitung', 'Bangka Belitung'),
    ('riau', 'Riau'),
    ('maluku', 'Maluku'),
    ('maluku_utara', 'Maluku_utara'),
    ('ntb', 'Nusa Tenggara Barat'),
    ('ntt', 'Nusa Tenggara Timur'),
    ('papua', 'Papua'),
    ('papua_barat', 'Papua Barat'),
    ('sulawesi_barat', 'Sulawesi Barat'),
    ('sulawesi_selatan', 'Sulawesi Selatan'),
    ('sulawesi_tengah', 'Sulawesi Tengah'),
    ('sulawesi_tenggara', 'Sulawesi Tenggara'),
    ('sumatra_barat', 'Sumatra Barat'),
    ('sumatra_selatan', 'Sumatra Selatan'),
    ('sumatra_utara', 'Sumatra Utara'),
    ('yogyakarta', 'Yogyakarta'),
    ]

class FormRS(forms.Form):
    nama = forms.CharField(label="Nama")
    hp = forms.CharField(label="Nomor HP")
    alamat = forms.CharField(label="Alamat")
    provinsi = forms.CharField(label="Provinsi" ,widget=forms.Select(choices=LIST_PROVINSI))

class FormProvinsi(forms.Form):
    provinsi = forms.CharField(label="", widget=forms.Select(choices=LIST_PROVINSI))