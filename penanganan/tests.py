from django.test import TestCase, Client

# Create your tests here.

class PenangananTest(TestCase):

    def test_index_url_exists(self):
        response = Client().get('/penanganan/')
        self.assertEquals(response.status_code, 200)

    def test_index_template_exists(self):
        response = Client().get('/penanganan/')
        self.assertTemplateUsed(response, 'penanganan/index.html')

    def test_add_url_exists(self):
        response = Client().get('/penanganan/add')
        self.assertEquals(response.status_code, 200)

    def test_add_template_exists(self):
        response = Client().get('/penanganan/add')
        self.assertTemplateUsed(response, 'penanganan/add.html')