from django.db import models

# Create your models here.
class RumahSakit(models.Model):
    nama = models.CharField(max_length=40)
    hp = models.CharField(max_length=40)
    alamat = models.CharField()
    provinsi = models.CharField(max_length=40)