from django.urls import path
from . import views

app_name = 'faq'

urlpatterns = [
    path('', views.index, name='index'),
    path('Add/', views.Add, name='Add'),
    path('Delete/<pk>/', views.Delete, name='Delete'),
    path('Edit/<int:idx>/', views.Edit, name='Edit'),

]