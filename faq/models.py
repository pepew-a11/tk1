from django.db import models

# Create your models here.
class FAQContent(models.Model):
    judul = models.TextField()
    isi = models.TextField()

    def __str__(self):
        return self.judul

