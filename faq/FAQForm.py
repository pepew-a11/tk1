from django.forms import ModelForm
from .models import *

class Content(ModelForm):
    class Meta:
        model = FAQContent
        fields = '__all__'

