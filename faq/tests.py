from django.test import TestCase, Client
from .models import FAQContent
# Create your tests here.

class FAQIndex(TestCase):
   #index.html 
    def test_url_exist(self):
        response = Client().get('/faq/')
        self.assertEquals(response.status_code, 200)

    def test_template_exist(self):
        response = Client().get('/faq/')
        self.assertTemplateUsed(response, "faq/index.html")

    def test_title_exist(self):
        response = Client().get('/faq/')
        html_response = response.content.decode('utf8')
        self.assertIn("FAQ", html_response)
    
    def test_model_exists(self):
        konten = FAQContent.objects.create(judul="bisa yuk", isi="bisa")
        self.assertEquals(FAQContent.objects.all().count(), 1)
    
    def test_model_and_button_shown(self):
        konten = FAQContent.objects.create(judul="Bisa yuk", isi="Bisa")
        response = Client().get('/faq/')
        html_response = response.content.decode('utf8')
        self.assertIn("Edit", html_response)
        self.assertIn("Delete", html_response)
    
    def test_add_url_exists(self):
        response = Client().get('/faq/Add/')
        self.assertEquals(response.status_code, 200)
        
    def test_add_template_exists(self):
        response = Client().get('/faq/Add/')
        self.assertTemplateUsed(response, 'faq/Add.html')

    def test_add_form_and_button_exists(self):
        response = Client().get('/faq/Add/')
        html_response = response.content.decode('utf8')
        self.assertIn("judul", html_response)
        self.assertIn("isi", html_response)

    def test_update_url_exists(self):
        konten=FAQContent.objects.create(judul="Bisa yuk", isi="yuk bisaa")
        response = Client().get('/faq/Edit/' + str(konten.id) + '/')
        self.assertEquals(response.status_code, 200)
        
    def test_update_tempalte_exists(self):
        konten=FAQContent.objects.create(judul="Bisa yuk", isi="yuk bisaa")
        response = Client().get('/faq/Edit/' + str(konten.id) + '/')
        self.assertTemplateUsed(response, 'faq/Edit.html')

    def test_update_form_and_button_exists(self):
        konten=FAQContent.objects.create(judul="Bisa yuk", isi="yuk bisaa")

        response = Client().get('/faq/Edit/' + str(konten.id) + '/')
        html_response = response.content.decode('utf8')
        self.assertIn("Edit", html_response)

    def test_user_save_info(self):
        Client().post('/faq/Add/', {'judul':'ppw itu mudah', 'isi':'ppw itu susah'})
        self.assertEquals(FAQContent.objects.all().count(), 1)

        response = Client().get('/faq/')
        html_response = response.content.decode('utf8')
        self.assertIn("mudah", html_response)
        self.assertIn("susah", html_response)

    def test_user_delete_info(self):
        konten = FAQContent.objects.create(judul="bisa yuk", isi="bisa")

        self.assertEquals(FAQContent.objects.all().count(), 1)

        Client().get('/faq/Delete/' + str(konten.id) + '/')
        #self.assertEquals(FAQContent.objects.all().count(), 0)

        #response = Client().get('/faq/')
        #html_response = response.content.decode('utf8')
        #self.assertNotIn("bisa", html_response)
        #self.assertNotIn("yuk", html_response)

    def test_user_update_info(self):
        konten = FAQContent.objects.create(judul="bisa yuk", isi="bisa")
        self.assertEquals(FAQContent.objects.all().count(), 1)

        response = Client().post('/faq/Add/' + str(konten.id) + '/', {'judul':'bisa yuk', 'isi':'bisa'})
        self.assertEquals(FAQContent.objects.all().count(), 1)

        response = Client().get('/hoaxbuster/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("bisa", html_response)
        self.assertNotIn("yuk", html_response)



        
        
    
