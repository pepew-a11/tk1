from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .FAQForm import Content

# Create your views here.

def index(request):
     FAQ = FAQContent.objects.all()
     return render(request, 'faq/index.html', {'item' :FAQ })

def Add(request):
    form = Content()
    if request.method == 'POST':
        form = Content(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form':form}
    return render(request, 'faq/Add.html', context)

def Delete(request, pk):
    konten = FAQContent.objects.filter(id=pk)
    judul = FAQContent.objects.get(id= pk)
    if request.method =='POST':
        konten.delete()
        return redirect('/faq/')
    context = {'form':konten}
    return render(request, 'faq/index.html' , context)

def Edit(request, idx):
    content = FAQContent.objects.get(id=idx)
    form = Content(instance=content)

    context= {'form': form}

    if request.method == 'POST': 
        form = Content(request.POST, request.FILES, instance=content)
  
        if form.is_valid():
            form.save()
        
        return redirect('/faq/')

    return render(request, 'faq/Edit.html', context)

    