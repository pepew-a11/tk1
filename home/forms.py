from django import forms
from .models import Data

class DataForm(forms.ModelForm):
    class Meta:
        model = Data
        fields = ['data_ODP','data_PDP','data_sembuh','data_kematian']