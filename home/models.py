from django.db import models

# Create your models here.
class Data(models.Model):
    data_ODP = models.IntegerField()
    data_PDP = models.IntegerField()
    data_sembuh = models.IntegerField()
    data_kematian = models.IntegerField()