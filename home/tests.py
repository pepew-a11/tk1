from django.test import TestCase, tag,Client
from django.urls import reverse



class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('home:index'))
        self.assertEqual(response.status_code, 200)
    
    def test_index_template_exists(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'home/index.html')

    def test_index_title_exists(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Home",html_response)

    def test_ubahindex_url_status_200(self):
        response = self.client.get(reverse('home:indexubah'))
        self.assertEqual(response.status_code, 200)

    def test_indexubah_title_exists(self):
        response = Client().get(reverse('home:indexubah'))
        html_response = response.content.decode('utf8')
        self.assertIn("Home ubah Data",html_response)

    def test_indexubah_template_exists(self):
        response = Client().get(reverse('home:indexubah'))
        self.assertTemplateUsed(response,'home/indexubah.html')




