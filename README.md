####################################################################################################

Anggota Kelompok:
- 1906398515 Kevin Dharmawan
- 1906398490 Rila Bagus Mustofa
- 1906398774 Mu'adz
- 1906398830 Muhammad Ikhsan Abdillah

####################################################################################################

https://tk1-a11.herokuapp.com
https://wireframe.cc/91y4qv
https://www.figma.com/file/iXDlchl3D7xaKlIancAtgv/Untitled?node-id=0%3A1

####################################################################################################

Home Page (‘/’)
Menjelaskan pandemi dan virus secara umum dan sejumlah informasi singkat tentang COVID-19

FAQ (‘/faq’)
Hal-hal yang sering ditanyakan mengenai COVID-19

Hoax Buster (‘/hoax’)
Fakta tentang informasi salah yang umum beredar di masyarakat

Penanganan COVID-19 (‘/penanganan’)
Membahas tentang gejala-gejala covid dan penanganan secara umum

####################################################################################################