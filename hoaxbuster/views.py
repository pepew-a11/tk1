from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .models import Info
from .forms import InfoForm

# Create your views here.

def index(request):
    infos = Info.objects.all()
    context = {'infos':infos}
    
    return render(request, 'hoaxbuster/index.html', context)

def updateinfo(request, info_id):
    info = Info.objects.get(id=info_id)
    form = InfoForm(instance=info)
    context = {'form':form}

    if request.method == 'POST': 
        form = InfoForm(request.POST, request.FILES, instance=info)
  
        if form.is_valid():
            form.save()
        
        return redirect('/hoaxbuster/')

    return render(request, 'hoaxbuster/updateinfo.html', context)

def addinfo(request):
    form = InfoForm()
    context = {'form':form}

    if request.method == 'POST': 
        form = InfoForm(request.POST, request.FILES)
  
        if form.is_valid():
            form.save()
        
        return redirect('/hoaxbuster/')

    return render(request, 'hoaxbuster/addinfo.html', context)

def deleteinfo(request, info_id):
    info = Info.objects.get(id=info_id)
    info.delete()

    return redirect('/hoaxbuster/')