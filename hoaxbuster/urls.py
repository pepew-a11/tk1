from django.urls import path

from . import views

app_name = 'hoaxbuster'

urlpatterns = [
    path('', views.index, name='index'),
    path('addinfo', views.addinfo, name='addinfo'),
    path('deleteinfo/<int:info_id>', views.deleteinfo, name='deleteinfo'),
    path('updateinfo/<int:info_id>', views.updateinfo, name='updateinfo'),
]
