from django.apps import AppConfig


class HoaxbusterConfig(AppConfig):
    name = 'hoaxbuster'
