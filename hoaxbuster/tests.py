from django.test import TestCase, Client
from django.core.files.images import ImageFile

from .models import Info

# Create your tests here.

class HoaxBusterTest(TestCase):

    def test_index_url_exists(self):
        response = Client().get('/hoaxbuster/')
        self.assertEquals(response.status_code, 200)

    def test_index_template_exists(self):
        response = Client().get('/hoaxbuster/')
        self.assertTemplateUsed(response, 'hoaxbuster/index.html')

    def test_index_title_and_button_exists(self):
        response = Client().get('/hoaxbuster/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hoax Buster", html_response)
        self.assertIn("Add Info", html_response)

    def test_model_exists(self):
        info = Info.objects.create(hoax="ppw itu mudah", fakta="ppw itu susah")
        self.assertEquals(Info.objects.all().count(), 1)

    def test_model_and_button_shown(self):
        info = Info.objects.create(hoax="ppw itu mudah", fakta="ppw itu susah")

        response = Client().get('/hoaxbuster/')
        html_response = response.content.decode('utf8')
        self.assertIn("mudah", html_response)
        self.assertIn("susah", html_response)
        self.assertIn("Update", html_response)
        self.assertIn("Delete", html_response)

    def test_add_url_exists(self):
        response = Client().get('/hoaxbuster/addinfo')
        self.assertEquals(response.status_code, 200)
        
    def test_add_template_exists(self):
        response = Client().get('/hoaxbuster/addinfo')
        self.assertTemplateUsed(response, 'hoaxbuster/addinfo.html')

    def test_add_form_and_button_exists(self):
        response = Client().get('/hoaxbuster/addinfo')
        html_response = response.content.decode('utf8')
        self.assertIn("Hoax Buster", html_response)
        self.assertIn("Add Info", html_response)

    def test_update_url_exists(self):
        info = Info.objects.create(hoax="ppw itu mudah", fakta="ppw itu susah")
        response = Client().get('/hoaxbuster/updateinfo/' + str(info.id))
        self.assertEquals(response.status_code, 200)
        
    def test_update_tempalte_exists(self):
        info = Info.objects.create(hoax="ppw itu mudah", fakta="ppw itu susah")
        response = Client().get('/hoaxbuster/updateinfo/' + str(info.id))
        self.assertTemplateUsed(response, 'hoaxbuster/updateinfo.html')

    def test_update_form_and_button_exists(self):
        info = Info.objects.create(hoax="ppw itu mudah", fakta="ppw itu susah")

        response = Client().get('/hoaxbuster/updateinfo/' + str(info.id))
        html_response = response.content.decode('utf8')
        self.assertIn("Hoax Buster", html_response)
        self.assertIn("Update Info", html_response)

    def test_user_save_info(self):
        Client().post('/hoaxbuster/addinfo', {'hoax':'ppw itu mudah', 'fakta':'ppw itu susah'})
        self.assertEquals(Info.objects.all().count(), 1)

        response = Client().get('/hoaxbuster/')
        html_response = response.content.decode('utf8')
        self.assertIn("mudah", html_response)
        self.assertIn("susah", html_response)

    def test_user_delete_info(self):
        info = Info.objects.create(hoax="ppw itu mudah", fakta="ppw itu susah")

        self.assertEquals(Info.objects.all().count(), 1)

        Client().get('/hoaxbuster/deleteinfo/' + str(info.id))
        self.assertEquals(Info.objects.all().count(), 0)

        response = Client().get('/hoaxbuster/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("mudah", html_response)
        self.assertNotIn("susah", html_response)

    def test_user_update_info(self):
        info = Info.objects.create(hoax="ppw itu mudah", fakta="ppw itu susah")
        self.assertEquals(Info.objects.all().count(), 1)

        response = Client().post('/hoaxbuster/updateinfo/' + str(info.id), {'hoax':'sda itu menyenangkan', 'fakta':'sda itu menyebalkan'})
        self.assertEquals(Info.objects.all().count(), 1)

        response = Client().get('/hoaxbuster/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("mudah", html_response)
        self.assertNotIn("susah", html_response)
        self.assertIn("menyenangkan", html_response)
        self.assertIn("menyebalkan", html_response)